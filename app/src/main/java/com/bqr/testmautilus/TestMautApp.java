package com.bqr.testmautilus;

import android.app.Application;

import com.bqr.testmautilus.database.base.DatabaseHelper;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;

/**
 * Created by bovquier on 02.02.2016.
 */
public class TestMautApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Logger.init("Mautilus").logLevel(LogLevel.FULL);
        DatabaseHelper.getHelper(getApplicationContext());
    }
}
