package com.bqr.testmautilus.requests.robospice;

import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by artur on 2015-08-04.
 */
public abstract class BaseSpiceRequest<T> extends SpiceRequest<T> {

    public BaseSpiceRequest(Class<T> clazz) {
        super(clazz);
    }
}
