package com.bqr.testmautilus.requests.robospice;

import android.app.Application;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.string.InFileStringObjectPersister;

/**
 * Created by artur on 2015-08-04.
 */
public class BaseSpiceService extends SpiceService {


    public static final int MAX_REQUESTS_THREAD = 3;

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        InFileStringObjectPersister stringObjectPersister = new InFileStringObjectPersister( application );
        cacheManager.addPersister(stringObjectPersister);
        return cacheManager;
    }

    @Override
    public int getThreadCount() {
        return MAX_REQUESTS_THREAD;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.v("BaseSpiceService", "Starting service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v("BaseSpiceService","Starting service");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v("BaseSpiceService","Stopping service");
    }



    @Override
    public IBinder onBind(Intent intent) {
        Log.v("BaseSpiceService","Bound service");
        return super.onBind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        Log.v("BaseSpiceService","Rebound service");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.v("BaseSpiceService","Unbound service");
        return super.onUnbind(intent);
    }
}