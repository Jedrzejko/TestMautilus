package com.bqr.testmautilus.requests;

import android.os.Environment;
import android.util.Log;

import com.bqr.testmautilus.UnicodeBOMInputStream;
import com.bqr.testmautilus.model.Dealers;
import com.bqr.testmautilus.requests.robospice.BaseSpiceRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orhanobut.logger.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Writer;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by bovquier on 02.02.2016.
 */
public class DownloadMautilusJson extends BaseSpiceRequest<Dealers> {

    private static final String UTF8_BOM = "\uFEFF";
    private static final String UTF8_BOM_U = "\ufeff";
    OkHttpClient client = new OkHttpClient();

    public DownloadMautilusJson(Class<Dealers> clazz) {
        super(clazz);
    }

    @Override
    public Dealers loadDataFromNetwork() throws Exception {
        Request request = new Request.Builder()
                .url("http://workbench.mautilus.com/example/dealers.json")
                .build();

        Response response = client.newCall(request).execute();
        ResponseBody body = response.body();
        /* UnicodeBOMInputStream - this util was found in the internet - simple line.replace() didn't work with few combinations of 0xFEFF */
        UnicodeBOMInputStream ubis = new UnicodeBOMInputStream(body.byteStream());
        InputStreamReader isr = new InputStreamReader(ubis);
        BufferedReader br = new BufferedReader(isr);
        ubis.skipBOM();
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            total.append(line);
        }
        br.close();
        isr.close();
        ubis.close();
        String string = total.toString();
        ObjectMapper mapper = new ObjectMapper();
        Dealers dealers = mapper.readValue(string, Dealers.class);
        Logger.d("JSON" + dealers.toString());
        return dealers;
    }

    private void saveFile(String trim, String fileName) {
        File root = Environment.getExternalStorageDirectory();
        File outDir = new File(root.getAbsolutePath() + File.separator + "EZ_time_tracker");
        if (!outDir.isDirectory()) {
            outDir.mkdir();
        }
        try {
            if (!outDir.isDirectory()) {
                throw new IOException(
                        "Unable to create directory EZ_time_tracker. Maybe the SD card is mounted?");
            }
            File outputFile = new File(outDir, fileName);
            Writer writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(trim);
            Logger.d("Report successfully saved to: " + outputFile.getAbsolutePath());
            writer.close();
        } catch (IOException e) {
            Log.w("eztt", e.getMessage(), e);
            Logger.e(e, e.getMessage() + " Unable to write to external storage.");
        }
    }
}
