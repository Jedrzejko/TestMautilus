package com.bqr.testmautilus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.bqr.testmautilus.adapters.DealersAdapter;
import com.bqr.testmautilus.base.BaseTestMautilusActivity;
import com.bqr.testmautilus.database.DealerDao;
import com.bqr.testmautilus.model.Dealer;
import com.bqr.testmautilus.model.Dealers;
import com.bqr.testmautilus.requests.DownloadMautilusJson;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DealersActivity extends BaseTestMautilusActivity {

    public static final String DEALER = "dealer_intent_data";
    @Bind(R.id.dealers_list)
    RecyclerView dealrsList;

    private List<Dealer> dealers;
    private LinearLayoutManager mLayoutManager;
    private DealersAdapter mDealersAdapter;
    private DealerDao dealerDao;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dealers;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dealerDao = DealerDao.getInstance(getApplicationContext());
        dealrsList.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(this);
        dealrsList.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDealersAdapter = new DealersAdapter(this, dealerClickListener);
        getSpiceManager().execute(new DownloadMautilusJson(Dealers.class), new RequestListener<Dealers>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Logger.e(spiceException, spiceException.getMessage());
                DealersActivity.this.dealers = dealerDao.readAll();
                Toast.makeText(DealersActivity.this, "Loaded from DB. Error: " + spiceException.getMessage(), Toast.LENGTH_LONG).show();
                setupDealersList();
            }

            @Override
            public void onRequestSuccess(Dealers dealers) {
                DealersActivity.this.dealers = new ArrayList<>(dealerDao.persist(dealers.getDealers()));
                setupDealersList();
            }
        });
    }

    DealersAdapter.DealerClickListener dealerClickListener = new DealersAdapter.DealerClickListener() {
        @Override
        public void onDealerClicked(int position) {
            Dealer dealer = mDealersAdapter.getItem(position);
            Intent dealerDetails = new Intent(DealersActivity.this, DealersDetailsActivity.class);
            dealerDetails.putExtra(DEALER, dealer);
            startActivity(dealerDetails);
        }
    };

    private void setupDealersList() {
        Logger.i("Dealers: " + dealers.toString());
        mDealersAdapter.addAll(dealers);
        dealrsList.setAdapter(mDealersAdapter);
    }


}
