package com.bqr.testmautilus.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bqr.testmautilus.requests.robospice.BaseSpiceService;
import com.octo.android.robospice.SpiceManager;

import butterknife.ButterKnife;

/**
 * Created by bovquier on 02.02.2016.
 */
public abstract class BaseTestMautilusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
    }

    protected abstract int getLayoutId();

    private SpiceManager spiceManager = new SpiceManager(BaseSpiceService.class);

    @Override
    public void onStart() {
        spiceManager.start(getApplicationContext());
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
