package com.bqr.testmautilus.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bqr.testmautilus.R;
import com.bqr.testmautilus.adapters.holders.DealerViewHolder;
import com.bqr.testmautilus.model.Dealer;

import java.util.List;

/**
 * Created by bovquier on 03.02.2016.
 */
public class DealersAdapter extends RecyclerView.Adapter<DealerViewHolder>{
    private List<Dealer> items;
    private Context context;
    private DealerClickListener dealerClickListener;

    public DealersAdapter(Context context, DealerClickListener dealerClickListener) {
        super();
        this.context = context;
        this.dealerClickListener = dealerClickListener;
    }

    @Override
    public DealerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DealerViewHolder(context, inflateView(parent, R.layout.single_dealer_item), dealerClickListener);
    }

    @Override
    public void onBindViewHolder(DealerViewHolder holder, int position) {
        holder.bindValues(getItem(position), position);
    }

    public Dealer getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addAll(List<Dealer> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    protected View inflateView(ViewGroup parent, int layoutId) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
    }

    public interface DealerClickListener {
        public void onDealerClicked(int position);
    }
}
