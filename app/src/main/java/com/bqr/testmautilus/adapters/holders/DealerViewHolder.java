package com.bqr.testmautilus.adapters.holders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bqr.testmautilus.R;
import com.bqr.testmautilus.adapters.DealersAdapter;
import com.bqr.testmautilus.model.Dealer;

import butterknife.Bind;

/**
 * Created by bovquier on 03.02.2016.
 */
public class DealerViewHolder extends BaseRecyclerHolder<Dealer> implements View.OnClickListener {

    private final Context context;
    private final DealersAdapter.DealerClickListener clickListener;
    @Bind(R.id.name)
    TextView name;
    @Bind(R.id.address)
    TextView address;
    @Bind(R.id.phone)
    TextView phone;
    @Bind(R.id.container)
    LinearLayout container;
    private int position;

    public DealerViewHolder(Context context, View itemView, DealersAdapter.DealerClickListener clickListener) {
        super(itemView);
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public void bindValues(Dealer dealer, int position) {
        this.position = position;
        name.setText(dealer.getName());
        address.setText(manageString(dealer.getStreet(), false).concat(manageString(dealer.getCity(), false)).concat("\n")
                .concat(manageString(dealer.getZipcode(), false)).concat(manageString(dealer.getCountry(), true)));
        phone.setText(manageString(dealer.getTel(), false));
        container.setBackgroundColor(
                ContextCompat.getColor(context, position % 2 == 0 ? R.color.odd_item : R.color.even_item));
        container.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        clickListener.onDealerClicked(position);
    }
}
