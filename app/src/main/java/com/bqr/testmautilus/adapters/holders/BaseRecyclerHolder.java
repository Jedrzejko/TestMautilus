package com.bqr.testmautilus.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by bovquier on 03.02.2016.
 */
public abstract class BaseRecyclerHolder<T> extends RecyclerView.ViewHolder {
    public BaseRecyclerHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public abstract void bindValues(T itemId, int position);

    protected String manageString(String string, boolean last) {
        if (string == null || string.length() == 0) return "";
        else return string.concat(last ? "" : ", ");
    }
}