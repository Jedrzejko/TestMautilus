package com.bqr.testmautilus.database.base;

import android.content.Context;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedDelete;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by bovquier on 03.02.2016.
 */
public abstract class GenericDao<E, PK> implements Dao<E, PK> {

    protected Context context;

    protected GenericDao(Context context) {
        this.context = context;
    }

    @Override
    public E readByPrimaryKey(PK key) {
        if (key == null) {
            return null;
        } else {
            return lookupRtDao().queryForId(key);
        }
    }

    @Override
    public List<E> readAll() {
        return lookupRtDao().queryForAll();
    }

    @Override
    public E persist(E entity) {
        lookupRtDao().createOrUpdate(entity);
        return entity;
    }

    @Override
    public Collection<E> persist(final Collection<E> entities) {
        lookupRtDao().callBatchTasks(new Callable<Void>() {
            public Void call() throws Exception {
                for (E entity : entities) {
                    persist(entity);
                }
                return null;
            }
        });
        return entities;
    }

    @Override
    public boolean exists(PK id) {
        return lookupRtDao().idExists(id);
    }

    @Override
    public long count() {
        return lookupRtDao().countOf();
    }

    @Override
    public void remove(E entity) {
        lookupRtDao().delete(entity);
    }

    @Override
    public void remove(Collection<E> entity) {
        lookupRtDao().delete(entity);
    }

    @Override
    public void removeAll() {
        lookupRtDao().callBatchTasks(new Callable<Void>() {
            public Void call() throws Exception {
                PreparedDelete<E> prepare = lookupRtDao().deleteBuilder().prepare();
                lookupRtDao().delete(prepare);
                return null;
            }
        });
    }

    @Override
    public Collection<E> removeAllAndPersistNew(Collection<E> collection) {
        removeAll();
        return persist(collection);
    }

    @Override
    public void refresh(Collection<E> entities) {
        for (E entity : entities) {
            refresh(entity);
        }
    }

    @Override
    public void refresh(E entity) {
        lookupRtDao().refresh(entity);
    }

    @Override
    public void removeByPrimaryKey(PK id) {
        lookupRtDao().deleteById(id);
    }

    private com.j256.ormlite.dao.Dao lookupDao() throws SQLException {
        return DaoProvider.lookupDao(returnedClass(), context);
    }

    public RuntimeExceptionDao<E, PK> lookupRtDao() {
        return DaoProvider.lookupRuntimeDao(returnedClass(), context);
    }

    protected Class<E> returnedClass() {
        ParameterizedType parameterizedType = null;
        Class currentClass = getClass();
        while (parameterizedType == null) {
            try {
                parameterizedType =
                        (ParameterizedType) currentClass.getGenericSuperclass();
            } catch (ClassCastException cce) {
                currentClass = currentClass.getSuperclass();
                if (currentClass == null) {
                    throw new RuntimeException("The is no generic superclass with params!");
                }
            }
        }
        int firstGeneric = 0;
        return (Class<E>) parameterizedType.getActualTypeArguments()[firstGeneric];
    }
}