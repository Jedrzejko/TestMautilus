package com.bqr.testmautilus.database.base;

import java.util.Collection;
import java.util.List;

/**
 * Created by bovquier on 03.02.2016.
 */
public interface ReadableDao<E, PK> {
    E readByPrimaryKey(PK var1);

    List<E> readAll();

    long count();

    void refresh(Collection<E> var1);

    void refresh(E var1);

    boolean exists(PK var1);
}
