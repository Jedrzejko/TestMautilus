package com.bqr.testmautilus.database.base;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.misc.SqlExceptionUtil;
import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.support.CompiledStatement;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.TableInfo;
import com.j256.ormlite.table.TableUtils;
import com.orhanobut.logger.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by bovquier on 03.02.2016.
 */
public class OrmLiteTableUtils {
    private static final FieldType[] noFieldTypes = new FieldType[0];
    private static final String PRAGMA_COLUMN_NAME = "name";
    private static final String PRAGMA_QUERY_FORMAT = "PRAGMA table_info(%s)";
    private static final String EXIST_QUERY_FORMAT = "SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = \'%s\'";

    public OrmLiteTableUtils() {
    }

    public static <T> void createIfNotExists(ConnectionSource connectionSource, Class... dataClasses) {
        Class[] arr$ = dataClasses;
        int len$ = dataClasses.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Class dataClass = arr$[i$];

            try {
                TableUtils.createTableIfNotExists(connectionSource, dataClass);
            } catch (SQLException var7) {
                throw new RuntimeException("error while creating table for class " + dataClass, var7);
            }
        }

    }

    public static void addColumnsIfNotExists(SQLiteDatabase db, ConnectionSource connectionSource, Class... dataClasses) {
        Class[] arr$ = dataClasses;
        int len$ = dataClasses.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Class dataClass = arr$[i$];

            try {
                checkColumns(db, connectionSource, dataClass);
            } catch (SQLException var8) {
                throw new RuntimeException("error while alter table for class " + dataClass, var8);
            }
        }

    }

    private static int checkColumns(SQLiteDatabase db, ConnectionSource connectionSource, Class dataClass) throws SQLException {
        Object columnsToCreate = new ArrayList();
        TableInfo tableInfo = new TableInfo(connectionSource, (BaseDaoImpl)null, dataClass);
        if(tableExists(db, tableInfo)) {
            columnsToCreate = getFieldsToCreate(db, tableInfo);
            DatabaseConnection connection = connectionSource.getReadWriteConnection();

            try {
                ArrayList statements = new ArrayList();
                DatabaseType databaseType = connectionSource.getDatabaseType();
                alterTableStatements(databaseType, (List)columnsToCreate, statements);
                doStatements(connection, "alter", statements, false, databaseType.isCreateTableReturnsNegative(), databaseType.isCreateTableReturnsZero());
            } finally {
                connectionSource.releaseConnection(connection);
            }
        }

        return ((List)columnsToCreate).size();
    }

    private static List<FieldType> getFieldsToCreate(SQLiteDatabase db, TableInfo tableInfo) {
        List persistedColumns = getPersistedColumns(db, tableInfo.getTableName());
        ArrayList newFields = new ArrayList();
        FieldType[] arr$ = tableInfo.getFieldTypes();
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            FieldType field = arr$[i$];
            if(field != null && !persistedColumns.contains(field.getColumnName().toUpperCase())) {
                newFields.add(field);
            }
        }

        return newFields;
    }

    private static List<String> getPersistedColumns(SQLiteDatabase db, String tableName) {
        ArrayList columns = new ArrayList();
        String query = String.format(Locale.ENGLISH, "PRAGMA table_info(%s)", new Object[]{tableName});
        Cursor cur = null;

        try {
            cur = db.rawQuery(query, (String[])null);

            while(cur.moveToNext()) {
                int colIndex = cur.getColumnIndex("name");
                String colName = cur.getString(colIndex);
                columns.add(colName.toUpperCase());
            }
        } finally {
            if(cur != null) {
                cur.close();
            }

        }

        return columns;
    }

    private static boolean tableExists(SQLiteDatabase db, TableInfo<?, ?> tableName) {
        String existsQuery = String.format(Locale.ENGLISH, "SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = \'%s\'", new Object[]{tableName.getTableName()});
        boolean isExists = false;
        Cursor cur = null;

        try {
            cur = db.rawQuery(existsQuery, (String[])null);
            isExists = cur != null && cur.getCount() > 0;
        } finally {
            if(cur != null) {
                cur.close();
            }

        }

        return isExists;
    }

    private static void alterTableStatements(DatabaseType databaseType, List<FieldType> columnsToAdd, List<String> statements) throws SQLException {
        ArrayList queriesAfter = new ArrayList();
        ArrayList additionalArgs = new ArrayList();
        ArrayList statementsBefore = new ArrayList();
        ArrayList statementsAfter = new ArrayList();
        Iterator i$ = columnsToAdd.iterator();

        while(i$.hasNext()) {
            FieldType fieldType = (FieldType)i$.next();
            if(!fieldType.isForeignCollection()) {
                StringBuilder sb = new StringBuilder(256);
                sb.append("ALTER TABLE ");
                databaseType.appendEscapedEntityName(sb, fieldType.getTableName());
                sb.append(" ADD COLUMN ");
                String columnDefinition = fieldType.getColumnDefinition();
                if(columnDefinition == null) {
                    databaseType.appendColumnArg(fieldType.getTableName(), sb, fieldType, additionalArgs, statementsBefore, statementsAfter, queriesAfter);
                } else {
                    databaseType.appendEscapedEntityName(sb, fieldType.getColumnName());
                    sb.append(' ').append(columnDefinition).append(' ');
                }

                statements.add(sb.toString());
            }
        }

    }

    private static int doStatements(DatabaseConnection connection, String label, Collection<String> statements, boolean ignoreErrors, boolean returnsNegative, boolean expectingZero) throws SQLException {
        int stmtC = 0;

        for(Iterator i$ = statements.iterator(); i$.hasNext(); ++stmtC) {
            String statement = (String)i$.next();
            int rowC = 0;
            CompiledStatement compiledStmt = null;

            try {
                compiledStmt = connection.compileStatement(statement, StatementBuilder.StatementType.EXECUTE, noFieldTypes, -1);
                rowC = compiledStmt.runExecute();
                Logger.d("executed %s table statement changed %s rows: %s", new Object[]{label, Integer.valueOf(rowC), statement});
            } catch (SQLException var15) {
                if(!ignoreErrors) {
                    throw SqlExceptionUtil.create("SQL statement failed: " + statement, var15);
                }

                Logger.e(var15, "ignoring %s error for statement: %s", new Object[]{label, statement});
            } finally {
                if(compiledStmt != null) {
                    compiledStmt.close();
                }

            }

            if(rowC < 0) {
                if(!returnsNegative) {
                    throw new SQLException("SQL statement " + statement + " updated " + rowC + " rows, we were expecting >= 0");
                }
            } else if(rowC > 0 && expectingZero) {
                throw new SQLException("SQL statement updated " + rowC + " rows, we were expecting == 0: " + statement);
            }
        }

        return stmtC;
    }

    private void dropTables(ConnectionSource connectionSource, Class... entities) {
        Class[] arr$ = entities;
        int len$ = entities.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Class entity = arr$[i$];

            try {
                TableUtils.dropTable(connectionSource, entity, false);
            } catch (SQLException var8) {
                throw new RuntimeException("error while dropping table for class " + entity, var8);
            }
        }

    }
}
