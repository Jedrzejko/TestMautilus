package com.bqr.testmautilus.database.base;

import java.util.Collection;

/**
 * Created by bovquier on 03.02.2016.
 */
public interface ModifiableDao<E, PK> {
    E persist(E var1);

    Collection<E> persist(Collection<E> var1);

    void remove(E var1);

    void remove(Collection<E> var1);

    void removeAll();

    Collection<E> removeAllAndPersistNew(Collection<E> var1);

    void removeByPrimaryKey(PK var1);
}
