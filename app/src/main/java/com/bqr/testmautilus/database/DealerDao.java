package com.bqr.testmautilus.database;

import android.content.Context;

import com.bqr.testmautilus.database.base.GenericDao;
import com.bqr.testmautilus.model.Dealer;

/**
 * Created by bovquier on 03.02.2016.
 */
public class DealerDao extends GenericDao<Dealer, Integer> {
    private static DealerDao INSTANCE = null;

    protected DealerDao(Context context) {
        super(context);
    }

    public static DealerDao getInstance(Context appContext) {
        synchronized (DealerDao.class) {
            if (INSTANCE == null) {
                synchronized (DealerDao.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new DealerDao(appContext);
                    }
                }
            }
        }
        return INSTANCE;
    }
}
