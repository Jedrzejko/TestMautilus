package com.bqr.testmautilus.database.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.bqr.testmautilus.model.Dealer;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Created by bovquier on 03.02.2016.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final Class[] ENTITIES = {
            Dealer.class
    };
    private static DatabaseHelper INSTANCE;

    public DatabaseHelper(Context context) {
        super(context, Database.DB_NAME, null, Database.VERSION);
    }

    public static synchronized DatabaseHelper getHelper(Context context) {
        synchronized (DatabaseHelper.class) {
            if (INSTANCE == null) {
                synchronized (DatabaseHelper.class) {
                    if (INSTANCE == null) {
                        INSTANCE = OpenHelperManager.getHelper(context, DatabaseHelper.class);
                    }
                }
            }
        }
        return INSTANCE;
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        OrmLiteTableUtils.createIfNotExists(connectionSource, ENTITIES);
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        OrmLiteTableUtils.addColumnsIfNotExists(db, connectionSource, ENTITIES);
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
    }

    public interface Database {
        int VERSION = 1;
        String DB_NAME = "mautilus_test.db";
    }
}