package com.bqr.testmautilus.database.base;

/**
 * Created by bovquier on 03.02.2016.
 */
public interface Dao<E, PK> extends ModifiableDao<E, PK>, ReadableDao<E, PK> {
}


