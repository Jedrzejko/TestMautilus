package com.bqr.testmautilus.database.base;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bovquier on 03.02.2016.
 */
public abstract class DaoProvider {

    private static final ConcurrentHashMap<Class, ? extends RuntimeExceptionDao> runtimeDaos;

    private static final ConcurrentHashMap<Class, ? extends Dao> daos;

    static {
        runtimeDaos = new ConcurrentHashMap<Class, RuntimeExceptionDao>();

        daos = new ConcurrentHashMap<Class, Dao>();
    }

    public static <D extends RuntimeExceptionDao<T, ?>, T> D lookupRuntimeDao(Class<T> clazz, Context context) {
        if (runtimeDaos.contains(clazz)) {
            return (D) runtimeDaos.get(clazz);
        } else {
            DatabaseHelper helper = DatabaseHelper.getHelper(context);
            return (D) helper.getRuntimeExceptionDao(clazz);
        }
    }

    public static <D extends Dao<T, ?>, T> D lookupDao(Class<T> clazz, Context context) throws SQLException {
        if (daos.contains(clazz)) {
            return (D) daos.get(clazz);
        } else {
            DatabaseHelper helper = DatabaseHelper.getHelper(context);
            return (D) helper.getDao(clazz);
        }
    }
}
