package com.bqr.testmautilus.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bovquier on 02.02.2016.
 */

@DatabaseTable(tableName = Dealer.DB_FIELDS.TAB_NAME)
public class Dealer implements Parcelable {
    @DatabaseField(columnName = DB_FIELDS.ID, id = true)
    @JsonProperty(DB_FIELDS.ID)
    private String id;
    @DatabaseField(columnName = DB_FIELDS.NAME)
    @JsonProperty(DB_FIELDS.NAME)
    private String name;
    @DatabaseField(columnName = DB_FIELDS.STREET)
    @JsonProperty(DB_FIELDS.STREET)
    private String street;
    @DatabaseField(columnName = DB_FIELDS.CITY)
    @JsonProperty(DB_FIELDS.CITY)
    private String city;
    @DatabaseField(columnName = DB_FIELDS.ZIP_CODE)
    @JsonProperty(DB_FIELDS.ZIP_CODE)
    private String zipcode;
    @DatabaseField(columnName = DB_FIELDS.COUNTRY)
    @JsonProperty(DB_FIELDS.COUNTRY)
    private String country;
    @DatabaseField(columnName = DB_FIELDS.TEL)
    @JsonProperty(DB_FIELDS.TEL)
    private String tel;
    @DatabaseField(columnName = DB_FIELDS.EMAIL)
    @JsonProperty(DB_FIELDS.EMAIL)
    private String email;
    @DatabaseField(columnName = DB_FIELDS.WWW)
    @JsonProperty(DB_FIELDS.WWW)
    private String www;
    @DatabaseField(columnName = DB_FIELDS.GPS_LAT)
    @JsonProperty(DB_FIELDS.GPS_LAT)
    private String gpsLat;
    @DatabaseField(columnName = DB_FIELDS.GPS_LON)
    @JsonProperty(DB_FIELDS.GPS_LON)
    private String gpsLon;
    @DatabaseField(columnName = DB_FIELDS.MAP_ICON)
    @JsonProperty(DB_FIELDS.MAP_ICON)
    private String mapIcon;

    /**
     * No args constructor for use in serialization
     */
    public Dealer() {
    }

    /**
     * @param id
     * @param email
     * @param gpsLat
     * @param tel
     * @param zipcode
     * @param street
     * @param name
     * @param www
     * @param mapIcon
     * @param gpsLon
     * @param country
     * @param city
     */
    public Dealer(String id, String name, String street, String city, String zipcode, String country, String tel, String email, String www, String gpsLat, String gpsLon, String mapIcon) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.city = city;
        this.zipcode = zipcode;
        this.country = country;
        this.tel = tel;
        this.email = email;
        this.www = www;
        this.gpsLat = gpsLat;
        this.gpsLon = gpsLon;
        this.mapIcon = mapIcon;
    }

    /**
     * @return The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The street
     */
    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The zipcode
     */
    @JsonProperty("zipcode")
    public String getZipcode() {
        return zipcode;
    }

    /**
     * @param zipcode The zipcode
     */
    @JsonProperty("zipcode")
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * @return The country
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The tel
     */
    @JsonProperty("tel")
    public String getTel() {
        return tel;
    }

    /**
     * @param tel The tel
     */
    @JsonProperty("tel")
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The www
     */
    @JsonProperty("www")
    public String getWww() {
        return www;
    }

    /**
     * @param www The www
     */
    @JsonProperty("www")
    public void setWww(String www) {
        this.www = www;
    }

    /**
     * @return The gpsLat
     */
    @JsonProperty("gps_lat")
    public String getGpsLat() {
        return gpsLat;
    }

    /**
     * @param gpsLat The gps_lat
     */
    @JsonProperty("gps_lat")
    public void setGpsLat(String gpsLat) {
        this.gpsLat = gpsLat;
    }

    /**
     * @return The gpsLon
     */
    @JsonProperty("gps_lon")
    public String getGpsLon() {
        return gpsLon;
    }

    /**
     * @param gpsLon The gps_lon
     */
    @JsonProperty("gps_lon")
    public void setGpsLon(String gpsLon) {
        this.gpsLon = gpsLon;
    }

    /**
     * @return The mapIcon
     */
    @JsonProperty("map_icon")
    public String getMapIcon() {
        return mapIcon;
    }

    /**
     * @param mapIcon The map_icon
     */
    @JsonProperty("map_icon")
    public void setMapIcon(String mapIcon) {
        this.mapIcon = mapIcon;
    }

    @Override
    public String toString() {
        return "Dealer{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", country='" + country + '\'' +
                ", tel='" + tel + '\'' +
                ", email='" + email + '\'' +
                ", www='" + www + '\'' +
                ", gpsLat='" + gpsLat + '\'' +
                ", gpsLon='" + gpsLon + '\'' +
                ", mapIcon='" + mapIcon + '\'' +
                '}';
    }

    public interface DB_FIELDS {
        String TAB_NAME = "dealers";
        String ID = "id";
        String NAME = "name";
        String STREET = "street";
        String CITY = "city";
        String ZIP_CODE = "zipcode";
        String COUNTRY = "country";
        String TEL = "tel";
        String EMAIL = "email";
        String WWW = "www";
        String GPS_LAT = "gps_lat";
        String GPS_LON = "gps_lon";
        String MAP_ICON = "map_icon";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.zipcode);
        dest.writeString(this.country);
        dest.writeString(this.tel);
        dest.writeString(this.email);
        dest.writeString(this.www);
        dest.writeString(this.gpsLat);
        dest.writeString(this.gpsLon);
        dest.writeString(this.mapIcon);
    }

    protected Dealer(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.street = in.readString();
        this.city = in.readString();
        this.zipcode = in.readString();
        this.country = in.readString();
        this.tel = in.readString();
        this.email = in.readString();
        this.www = in.readString();
        this.gpsLat = in.readString();
        this.gpsLon = in.readString();
        this.mapIcon = in.readString();
    }

    public static final Parcelable.Creator<Dealer> CREATOR = new Parcelable.Creator<Dealer>() {
        public Dealer createFromParcel(Parcel source) {
            return new Dealer(source);
        }

        public Dealer[] newArray(int size) {
            return new Dealer[size];
        }
    };
}