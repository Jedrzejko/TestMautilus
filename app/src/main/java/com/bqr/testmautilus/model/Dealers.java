package com.bqr.testmautilus.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bovquier on 02.02.2016.
 */
public class Dealers {

    @JsonProperty("dealers")
    private List<Dealer> dealers = new ArrayList<Dealer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Dealers() {
    }

    /**
     *
     * @param dealers
     */
    public Dealers(List<Dealer> dealers) {
        this.dealers = dealers;
    }

    /**
     *
     * @return
     * The dealers
     */
    @JsonProperty("dealers")
    public List<Dealer> getDealers() {
        return dealers;
    }

    /**
     *
     * @param dealers
     * The dealers
     */
    @JsonProperty("dealers")
    public void setDealers(List<Dealer> dealers) {
        this.dealers = dealers;
    }

    @Override
    public String toString() {
        return "Dealers{" +
                "dealers=" + dealers +
                '}';
    }
}