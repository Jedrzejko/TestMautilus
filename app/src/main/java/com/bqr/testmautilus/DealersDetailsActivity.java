package com.bqr.testmautilus;

import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

import com.bqr.testmautilus.base.BaseTestMautilusActivity;
import com.bqr.testmautilus.model.Dealer;

import butterknife.Bind;

/**
 * Created by bovquier on 03.02.2016.
 */
public class DealersDetailsActivity extends BaseTestMautilusActivity {

    private static final int CALL_REQUEST = 123;
    @Bind(R.id.dealer_data_city)
    TextView city;
    @Bind(R.id.dealer_data_country)
    TextView country;
    @Bind(R.id.dealer_data_gpsLat)
    TextView gpsLat;
    @Bind(R.id.dealer_data_gpsLon)
    TextView gpsLon;
    @Bind(R.id.dealer_data_name)
    TextView name;
    @Bind(R.id.dealer_data_street)
    TextView street;
    @Bind(R.id.dealer_data_zip)
    TextView zipCode;
    @Bind(R.id.dealer_data_email)
    TextView email;
    @Bind(R.id.dealer_data_phone)
    TextView phone;
    @Bind(R.id.dealer_data_web)
    TextView web;
    private Dealer dealer;

    @Override
    protected int getLayoutId() {
        return R.layout.deatils_activity;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        dealer = getIntent().getParcelableExtra(DealersActivity.DEALER);
        if (dealer == null) {
            finish();
        } else {
            city.setText(dealer.getCity());
            country.setText(dealer.getCountry());
            gpsLat.setText(dealer.getGpsLat());
            gpsLon.setText(dealer.getGpsLon());
            name.setText(dealer.getName());
            street.setText(dealer.getStreet());
            zipCode.setText(dealer.getZipcode());
            email.setText(dealer.getEmail());
            Linkify.addLinks(email, Linkify.EMAIL_ADDRESSES);
            phone.setText(dealer.getTel());
            Linkify.addLinks(phone, Linkify.PHONE_NUMBERS);
            web.setText(dealer.getWww());
            Linkify.addLinks(web, Linkify.WEB_URLS);
        }
    }
}
